#include "Asteroid.h"
#include <time.h>
#include "Globals.h"

DLinkedList<Asteroid> Asteroid::asteroidList;
DListIterator<Asteroid> Asteroid::asteroidListItr;
Asteroid::Asteroid()
{
	srand ( time(NULL) );
	positionSet=false;
	oldTime=SDL_GetTicks();
}
Asteroid::~Asteroid()
{

}
void Asteroid::doDraw(SDL_Surface* Surf_Display)
{
	if(Surf_Entity == NULL || Surf_Display == NULL) return;
		Weapon::listItr.Start();
	bool onscreen = onScreen();

	if(!positionSet)
	{
		X = rand()% 800;
		int y = rand() % 1500 +50;
		Y = -1*y;
		positionSet=true;
	}

	if(onscreen)
	{
	Sprite::OnDraw(Surf_Display, Surf_Entity,X, Y, CurrentFrameCol * Width, (CurrentFrameRow +Anim_Control.GetCurrentFrame()) * Height, Width, Height);
	}

	if(Y > ScreenHeight)
	{
		positionSet=false;
	}
	if(onCollision(g_hero.getBounds()))
	{
		g_hero.setArmour(-60);
		g_hero.setScore(-10);
		positionSet=false;
	}
	while(Weapon::listItr.Valid())
	{
		if(Weapon::listItr.Item()->onCollision(getBounds()))
		{
			positionSet=false;
			Weapon::activeBullet.Remove(Weapon::listItr);
			g_hero.setScore(50);
		}
		Weapon::listItr.Forth();
	}

	if(positionSet)
		Y+=0.7f + (scrollspeed/8);
	Animate();
		
}
bool Asteroid::timePassed(float timePass)
{
	float desiredTime = timePass*1000;
	if(SDL_GetTicks()-oldTime>desiredTime)
	{
		oldTime = SDL_GetTicks();
		return true;
	}
	return false;
}