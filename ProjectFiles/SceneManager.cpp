
#include "SceneManager.h"


SceneManager::SceneManager()
{
}

SceneManager::~SceneManager()
{
}
void SceneManager::deinitialize()
{
}

bool SceneManager::initialize()
{
	//_game_scene.initialise();
	_context = CTX_LOGIN;
	_login.initialise();
	_isInitialized[CTX_LOGIN] = true;
		
	return true;
}

void SceneManager::handleEvents(float timeScale)
{
	switch(_context)
	{
		case CTX_LOGIN:
			_login.handleEvents();
			break;
		case CTX_GAME:
			_game_scene.handleEvents(timeScale);
			break;
		case CTX_MENU:
			_menu.handleEvents();
			break;
		case CTX_BRIEFING:
			_briefing.handleEvents();
			break;
		case CTX_QUIT:
			break;
	}

	//_cursor.setPosition(eventReceiver.mouseX(), eventReceiver.mouseY());
}

void SceneManager::update(float timeScale)
{
	switch(_context)
	{
		case CTX_LOGIN:
			_login.update();
			break;
		case CTX_GAME:
			_game_scene.update(timeScale);
			break;
		case CTX_MENU:
			_menu.update();
			break;
		case CTX_BRIEFING:
			_briefing.update();
			break;
		case CTX_QUIT:
			_game_scene.saveInfo();
			//_game_scene.deinitialise();
			break;
	}
}

void SceneManager::draw(float timeScale)
{
	switch(_context)
	{
		case CTX_LOGIN:
			_login.draw();
			break;
		case CTX_GAME:
			_game_scene.draw(timeScale);
			break;
		case CTX_MENU:
		 	_menu.draw();
			break;
		case CTX_BRIEFING:
			_briefing.draw();
			break;
		case CTX_QUIT:
			break;
	}

}

void SceneManager::setContext(ContextID context) 
{ 
	_context = context;
	//_isInitialized[context]=false;
	if(!_isInitialized[context])
	{
		switch(_context)
		{
			case CTX_LOGIN:
			_login.initialise();
				_isInitialized[CTX_LOGIN] = true;
				break;
			case CTX_GAME:
				_game_scene.initialise();
				_isInitialized[CTX_GAME] = true;
				break;
			case CTX_MENU:
				_menu.initialise();
				_isInitialized[CTX_MENU] = true;
				break;
			case CTX_BRIEFING:
				_briefing.initialise();
				_isInitialized[CTX_BRIEFING]=true;
				break;
			case CTX_QUIT:
				_isInitialized[CTX_QUIT]=true;
				break;
		}
	}
}