#ifndef _LOGIN_H
#define _LOGIN_H
#include "Sprite.h"
#include "StringInput.h"
#include "Array.h"
#include "HashTable.h"
#include "Entity.h"
#include <SDL_mixer.h>

class Login
{
public:
	Login():trackList(3){}
	~Login();
	
	bool initialise();
	void handleEvents();
	void deinitialise();
	void draw();
	void update();
	bool loadData();
	bool doLogin();
	bool doRegister();
	void drawLogin();
	void drawRegister();
	void drawMenu();
	bool timePassed(Uint32,long&);
	//boolean flags
	bool loginScreen;//if user is on login page
	bool showInfo;
	bool removeInfo;
	bool registerScreen;//if user is on register page
	bool volChange;//if user is changing volume
	bool nameEntered;//if there is a value for name
	bool passEntered;//if there is a value for password
	bool failed;//if the login failed
	bool regSuccess;
	bool regFailed;
	bool noUsernameFound;//if the username was not found
	bool wrongPass;//if the password did not match the user found with a  name

	float musicVol;
	int musicTrack;
	long newTime;

	StringInput name;
	StringInput password;
private:
	Array<Mix_Music*> trackList;
	SDL_Surface* login_Bg;
	//SDL_Surface* title;
	SDL_Surface* nameMsg;
	SDL_Surface* passMsg;
	SDL_Surface* loginFailMsg;
	SDL_Surface* wrongPassMsg;
	SDL_Surface* userNotFoundMsg;
	SDL_Surface* simpleMenu;
	SDL_Surface* menuHeading;
	SDL_Surface* volLabel;
	Entity infoScreen;
	Entity title;
	TTF_Font* headerFont;
	Mix_Chunk* changeMenu;
	Mix_Chunk* errorFX;
	Mix_Music *track1;
	Mix_Music *track2;
	Mix_Music *track3;
};

#endif