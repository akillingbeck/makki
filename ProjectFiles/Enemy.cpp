#include "Enemy.h"
#include "Weapon.h"
#include <time.h>
long oldTime;
bool timePassed(float);
Enemy::Enemy()
{
	Weapon::listItr = Weapon::activeBullet.GetIterator();
	health=100;
	armour=50;
	oldTime = SDL_GetTicks();
	srand ( time(NULL) );
}
Enemy::~Enemy()
{

}
void Enemy::doDraw(SDL_Surface* Surf_Display,float timeScale)
{
	collision=false;
	Weapon::listItr.Start();
	if(Surf_Entity == NULL || Surf_Display == NULL) return;

		if(g_hero.onCollision(getBounds()))
		{
			CurrentFrameCol=1;
			collision=true;
			g_hero.setArmour(-30);
			g_hero.setScore(-30);
			setArmour(-50);
			if(Y > 0)
			Y -= Height;

		}
		while(Weapon::listItr.Valid())
		{
			if(Weapon::listItr.Item()->onCollision(getBounds()))
			{
				//Y = -Height;
				//X = rand() % 700 +50;
				g_hero.setScore(10);
				CurrentFrameCol=1;
				collision=true;
				setArmour(-Weapon::listItr.Item()->getDamage());
				Weapon::activeBullet.Remove(Weapon::listItr);
			}
			Weapon::listItr.Forth();
		}
		
		if(Y > ScreenHeight)
		{
			Y = -Height;
			X = rand() % 700 +50;
			health=100;
			armour=50;
		}
		if((X+Width)/2 < (g_hero.X+g_hero.Width)/2)
			X+=0.6f*timeScale;
		else if((X+Width)/2 > (g_hero.X+g_hero.Width)/2)
			X-=0.6f*timeScale;

		Sprite::OnDraw(Surf_Display, Surf_Entity,X, Y, CurrentFrameCol * Width, (CurrentFrameRow +Anim_Control.GetCurrentFrame()) * Height, Width, Height);
		Animate();

		Y+=0.1f;//*timeScale + (scrollspeed/8);
		if(!collision && timePassed(0.2))
			CurrentFrameCol=0;
}
void Enemy::setArmour(int p_armour)
{
	armour += p_armour;
	if(armour>0)
	{
		setHealth(p_armour/2);
	}
	else if(armour < 0)
	{
		armour=0;
		setHealth(p_armour);
	}
}
void Enemy::setHealth(int p_health)
{
	health += p_health;
	if(health<0)
	{
		Y = -Height;
		X = rand() % 700 +50;
		health=100;
		g_hero.setScore(80);
	}
	else
	{
	
	}
}
bool timePassed(float timePass)
{
	float desiredTime = timePass*1000;
	if(SDL_GetTicks()-oldTime>desiredTime)
	{
		oldTime = SDL_GetTicks();
		return true;
	}
	return false;
}