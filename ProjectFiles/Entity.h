#ifndef _ENTITY_H_
#define _ENTITY_H_

#include "Animation.h"
#include "Sprite.h"
#include "Defines.h"
#include "DLinkedList.h"
#include <vector>


class Entity {
    protected:

        Animation      Anim_Control;

        SDL_Surface*    Surf_Entity;
		SDL_Rect bounds;

    public:
		//static DLinkedList<Entity> entityList;
		//static DListIterator<Entity> entityItr;
		int             CurrentFrameCol;
		int             CurrentFrameRow;
        float           X;
        float           Y;

        int             Width;
        int             Height;
        int             AnimState;
protected:
    public:
        Entity();
        virtual ~Entity();
		SDL_Surface* getSurf();
public:
	//sets
	void setAnimFrame(int);
	void OscillateAnim(bool);
	void setAnimFrameRate(int frame);
	int getAnimFrame();

	//gets
	inline int getMaxFrames(){return Anim_Control.getMaxFrames();}
	SDL_Rect getBounds();
	bool onScreen();
    public:
        virtual bool doLoad(char* File, int Width, int Height, int MaxFrames);
	
        virtual void Animate();

        virtual void doDraw(SDL_Surface* Surf_Display,bool animate=true);
        virtual void doCleanup();
		virtual bool onCollision(SDL_Rect);//, Entity &ent2);
};
 
#endif