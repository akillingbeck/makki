#ifndef __CHAR_H
#define __CHAR_H

#include <string>
#include "Entity.h"
#include "Defines.h"
//#include "Weapon.h"
class Char : public Entity
{
public:

	// Create/destroy
	Char();
	~Char();
	//get move speed
	float getMoveSpeed();

	// Actions
	void moveLeft(float);
	void moveRight(float);
	void moveUp(float);
	void moveDown(float);
	void fire(float);
	void stop();

protected:
	float moveSpeed,acceleration,velocity,xPos,yPos;
};

#endif // __CHAR_H