
#include "makki.h"
#include "Globals.h"
#include "SceneManager.h"
#include <SDL_mixer.h>
#include <string>


makki::makki()
{
	g_SrWindow = 0;

}
makki::~makki()
{
}
void makki::run()
{
	int lastFPS = -1;
	int cycles = 0;

	//the "last" time
	Uint32 timeStamp = SDL_GetTicks();
	while(quit==false)
	{
		cycles++;
		 const Uint32 current = SDL_GetTicks();
         const float deltaTime = ((current - timeStamp) / 1000.f)*FRAMES_PER_SECOND; // Time in seconds
        timeStamp = current;

		//events.handleEvents();
		sceneManager.handleEvents(deltaTime);
		sceneManager.draw(deltaTime);
		sceneManager.update(deltaTime);
	}
	exit();
}
bool makki::initialise()
{
	  //init everything
    if(SDL_Init(SDL_INIT_EVERYTHING)== -1)
    {
    return false;
    }
    //setup screen
    g_SrWindow = SDL_SetVideoMode(ScreenWidth,ScreenHeight,BPP,SDL_SWSURFACE);
    //test screen for NULL
    if(g_SrWindow==NULL)
    return false;

	if( TTF_Init() == -1 ) 
	{ 
		return false; 
	}

	  font = TTF_OpenFont( "..//Data//Login//ARIAL.ttf", 28 );
	  if(font==NULL)
	  {
		  return false;
	  }
	  SDL_EnableUNICODE(SDL_ENABLE);
	if( Mix_OpenAudio( 22050, MIX_DEFAULT_FORMAT, 2, 4096 ) == -1 ) { return false; }
    //set window caption
    SDL_WM_SetCaption(PROGRAM_NAME,NULL);
	SDL_Surface* icon = SDL_LoadBMP("..//Data//Icon.bmp");
		SDL_WM_SetIcon(icon, NULL);

    return sceneManager.initialize();


}
void makki::exit()
{
	SDL_FreeSurface(g_SrWindow);
	TTF_CloseFont(font);
	Mix_CloseAudio();
	  SDL_Quit();
}

int main(int argc, char* args[])
{
	makki Makki;
	if(!Makki.initialise())
		return -1;
	Makki.run();
	return 0;

}