#ifndef _ITEM_H_
#define _ITEM_H_
#include "Entity.h"
#include "Globals.h"
#include <SDL_mixer.h>
class Item : public Entity
{
public:
		Item();
		~Item();
	enum ItemType{
		ITM_Health=0,ITM_Armour,ITM_Weapon,ITM_Life,ITM_Size
	};
public:
	static bool setPosition;
	static bool drawItem;
	bool isLoaded;
public:
	void setSoundEffect(const char*);
	inline ItemType getType(){return _type;}
	void setType(ItemType);
	void doDraw(SDL_Surface* Surf_Display);
protected:
	ItemType _type;
	Mix_Chunk* soundEffect;

};
#endif
