#ifndef __SCENE_MANAGER_H__
#define __SCENE_MANAGER_H__

#include "Menu.h"
#include "MissionBriefing.h"
#include "Login.h"
#include "Game.h"

class SceneManager
{
public:
	enum ContextID {
		CTX_LOGIN,
		CTX_MENU,
		CTX_GAME,
		CTX_BRIEFING,
		CTX_QUIT,
		CTX_NUMBER
	};

	SceneManager();
	~SceneManager();

	bool initialize();
	void deinitialize();

	void setContext(ContextID context);

	void handleEvents(float);
	void update(float);
	void draw(float);

private:
	//Sprite2D _cursor;
	ContextID _context;
	bool _isInitialized[CTX_NUMBER];

	Login _login;
	MissionBriefing _briefing;
	Game _game_scene;
	Menu _menu;
};

#endif