#include <SDL.h>
#include "Game.h"
#include "Sprite.h"
#include "Defines.h"
#include "SceneManager.h"
#include "SDLHelpers.h"
#include "Asteroid.h"
#include <time.h>
#include <sstream>
#include <fstream>
#include <time.h>
//function prototypes
int mostNeededPwrUP();
/*-------------------------------------------------*/
Game::~Game()
{
	//deinitialise();
}
bool Game::initialise()
{
	int level=0;
		keystates = SDL_GetKeyState(0);
		gameOver=false;
if(!loadMap()||!loadData()||!loadPlayerRelated())
	{
		quit=true;
	}

//initialise variables

volLabel = TTF_RenderText_Solid(font,"VOLUME:",GREEN);
/*std::stringstream ss;
ss<<"Best Score: "<<g_hero.getPersonalBest();
personalBest = TTF_RenderText_Solid(font,ss.str().c_str(),GREEN);*/
	srand ( time(NULL) );

	musicTrack=0;
	soundVolume=20;

	asteroidwave=1;
	asteroidField=true;
	lastScore = g_hero.getScore();
	//bestScore = g_hero.getPersonalBest();
	Asteroid::asteroidListItr = Asteroid::asteroidList.GetIterator();
	//flags
	introDone=false;
	GameOverplaying=false;
	Item::drawItem=false;
	//sound
	Mix_PlayChannel(-1,doorAlert,0);
	Mix_Volume(-1,soundVolume);
	return true;
}
void Game::handleEvents(float timeScale)
{
	volChange=false;
	g_hero.CurrentFrameCol=1;
	if(keystates[SDLK_MINUS])
	{
		volChange=true;
		if(soundVolume>0)
		{
			soundVolume-=0.05f;
			Mix_Volume(-1,soundVolume);
			Mix_VolumeMusic(soundVolume);
		}
	}
	if(keystates[SDLK_EQUALS])
	{
		volChange=true;
		if(soundVolume<40)
		{
			soundVolume+=0.05f;
			Mix_Volume(-1,soundVolume);
			Mix_VolumeMusic(soundVolume);
		}
	}
	if(introDone && !gameOver)
	{
	if(keystates[SDLK_DOWN])
	{
		g_hero.moveDown(timeScale);
	}
	if(keystates[SDLK_UP])
	{
		g_hero.setAnimFrameRate(100);
		g_hero.moveUp(timeScale);
	if(scrollspeed<4)
		scrollspeed++;
	}
	if(keystates[SDLK_LEFT])
	{
		g_hero.CurrentFrameCol=0;
		g_hero.moveLeft(timeScale);
		//bgX+=9.4f*timeScale;
	}
	if(keystates[SDLK_RIGHT])
	{
		g_hero.CurrentFrameCol=2;
		g_hero.moveRight(timeScale);
	}
	}
	
	SDL_Event m_event;
	if( SDL_PollEvent( &m_event ) )
        {
			//MUSIC TRACK--------------------------------------------------
			if(m_event.type==SDL_KEYUP)
			{
				if(m_event.key.keysym.sym==SDLK_UP)
				{
					scrollspeed=2.4f;
				}
			}
			if(m_event.type == SDL_KEYDOWN)
			{
				if(m_event.key.keysym.sym==SDLK_PAGEUP)
				{
					if(musicTrack!=2)
					{
						musicTrack++;
					}
					Mix_PlayMusic(trackList[musicTrack],-1);
				}
				if(m_event.key.keysym.sym==SDLK_PAGEDOWN)
				{
					if(musicTrack!=0)
					{
						musicTrack--;
					}
					Mix_PlayMusic(trackList[musicTrack],-1);
				}
				//------------------------------END MUSIC TRACK--------------------------------------------------
				if(m_event.key.keysym.sym==SDLK_ESCAPE)
				{
					quit=true;
				//	g_hero.setPersonalBest(g_hero.getScore());
					if(gameOver)
					{
						g_hero.resetPlayer();
					}
					sceneManager.setContext(SceneManager::CTX_QUIT);
					//	sceneManager.setContext(SceneManager::CTX_MENU);
				}
				if(m_event.key.keysym.sym==SDLK_w)
				{
					g_hero.nextWeapon();
				}
				if(m_event.key.keysym.sym==SDLK_d)
				{
					g_hero.previousWeapon();
				}
				if(m_event.key.keysym.sym==SDLK_RETURN)
				{
					gameOver=false;
					GameOverplaying = false;
					//g_hero.setPersonalBest(g_hero.getScore());
					Mix_PlayMusic(trackList[musicTrack],-1);
					g_hero.resetPlayer();
				}
				if(m_event.key.keysym.sym==SDLK_SPACE)
				{
					if(introDone)
					g_hero.fire(timeScale);
				}
			}
			if(m_event.type==SDL_QUIT)
			{
				//saveInfo(g_hero);
				quit = true;
				//deinitialise();
			}
			
        }
}
void Game::update(float timeScale)
{
	if(introDone && !gameOver)
	{
		if(!Item::setPosition)
		{
			//set the initial position of each item, this means we have to loop through the array
			//and set the position, it will be O(n)
			//the position is only set when the items are needed
				for(int i=0;i<items.Size();i++)
				{
					items[i].X = rand() % 700 + 50;
					items[i].Y = -items[i].Height;
				}
				Item::setPosition=true;
		}
	g_hero.setAnimFrameRate(200);
	
	bgY+=scrollspeed*timeScale;
	spacebgY+=1*timeScale;
	}
	else if(gameOver)
	{
		
		if(!GameOverplaying)
		{
		Mix_PlayMusic(gameOverTR,-1);
		GameOverplaying=true;
		//g_hero.setPersonalBest(g_hero.getScore());
		}
	}

	SDL_Flip(g_SrWindow);
}

void Game::draw(float timeScale)
{	
	if(!gameOver)
	{
	if( bgY >= spaceBg->h || spacebgY >= spaceBg->h) { 
		spacebgY=0;
		bgY = 0; }

	if(introDone)
	{
	drawBG();
	drawEntities(timeScale);
	drawHUD();
	if(asteroidField)
	{
		Asteroid::asteroidListItr.Start();
		while(Asteroid::asteroidListItr.Valid())
		{

			Asteroid::asteroidListItr.Item().doDraw(g_SrWindow);
			Asteroid::asteroidListItr.Forth();
		}
		if(timePassed(15,astrOLD))
		{
			asteroidField=false;
		}
	}
	else
	{
		if(timePassed(35,astrOLD))
		{
			asteroidField=true;
		}
	}
	}
	else
	{
		
		introBMP.doDraw(g_SrWindow,true);
		g_hero.doDraw(g_SrWindow);
		if(introBMP.getAnimFrame()==8)
		{
			introDone=true;
			pwrupOLD=SDL_GetTicks();
			astrOLD=SDL_GetTicks();
			introBMP.doCleanup();
			Mix_FreeChunk(doorAlert);
		}
	}
	}
	else
	{
		Sprite::OnDraw(g_SrWindow,gameOverSurf,0,0);
	}
	if(volChange)
	{
		Sprite::OnDraw(g_SrWindow,volLabel,(ScreenWidth - volLabel->w)/2,(ScreenHeight - volLabel->h));
		SDLBox(g_SrWindow,(ScreenWidth - volLabel->w)/2 + (volLabel->w),(ScreenHeight - volLabel->h)+10,(int)soundVolume*5,14,GREEN);
	}
	//Sprite::OnDraw(g_SrWindow,nameTag,(g_hero.X + (g_hero.getSurf()->w)/2)-((nameTag->w)/2),g_hero.Y+g_hero.Height);
}
void Game::drawEntities(float timeScale)
{
	if(Item::drawItem)
	{
		//item will be chosen from array based on what is needed most
		//therefore complexity will be O(c)
		int index = mostNeededPwrUP();
		if(index!=-1)
		{
		items[index].doDraw(g_SrWindow);
		items[index].Y +=0.5f+(scrollspeed/8);
		}
	}
	enemy.doDraw(g_SrWindow,timeScale);
	/*enemyItr.Start();
	while(enemyItr.Valid())
	{
		enemyItr.Item().doDraw(g_SrWindow);
		enemyItr.Forth();
	}*/
	if(timePassed(30,pwrupOLD))
	{
		if(mostNeededPwrUP()!=-1)
		{
		Item::drawItem=true;
		}
	}
	
	g_hero.doDraw(g_SrWindow);
}
bool Game::timePassed(Uint32 timePass, long &oldtime)
{
	Uint32 desiredTime = timePass*1000;
	if(SDL_GetTicks()-oldtime>desiredTime)
	{
		oldtime = SDL_GetTicks();
		return true;
	}
	return false;
}
void Game::drawHUD()
{
	
	Sprite::OnDraw(g_SrWindow,playerStatusBar,ScreenWidth-playerStatusBar->w,ScreenHeight-playerStatusBar->h);
	SDLBox(g_SrWindow,(ScreenWidth-(playerStatusBar->w-45)),(ScreenHeight-(playerStatusBar->h-15)),g_hero.getHealth()/2,10,GREEN);
	SDLBox(g_SrWindow,(ScreenWidth-(playerStatusBar->w-45)),(ScreenHeight-(playerStatusBar->h-40)),g_hero.getArmour()/2,10,BLUE);
	std::stringstream scoreText;
	scoreText<<"SCORE: "<<g_hero.getScore();
	if(lastScore!=g_hero.getScore())
	{
	SDL_FreeSurface(scoreBoard);
	scoreBoard = TTF_RenderText_Solid(font,scoreText.str().c_str(),GREEN);
	lastScore = g_hero.getScore();

	}
/*	if(bestScore!=g_hero.getPersonalBest())
	{
		SDL_FreeSurface(personalBest);
		std::stringstream ss;
		ss<<"Best Score: "<<g_hero.getPersonalBest();
		personalBest = TTF_RenderText_Solid(font,ss.str().c_str(),GREEN);
	}*/
	//Sprite::OnDraw(g_SrWindow,personalBest,0,personalBest->h);
	Sprite::OnDraw(g_SrWindow,scoreBoard,0,0);
	//the lives are held in an array of size 5
	//each loop it iterates though the array and draws each. O(n)
	//I could also have had 1 single image which containted all 5 lives in a row
	//and clip how many lives were needed from 1 single image, this would mean
	//I would not need a loop
	for(int i=0;i<g_hero.getLives();i++)
	{
		lives[i].doDraw(g_SrWindow);
	}
}
void Game::drawBG()
{
	Sprite::OnDraw(g_SrWindow,spaceBg,0,spacebgY);
	Sprite::OnDraw(g_SrWindow,spaceBg,0,spacebgY-ScreenHeight);
	Sprite::OnDraw(g_SrWindow,spaceOver,0,bgY);
	Sprite::OnDraw(g_SrWindow,spaceOver,0,bgY-ScreenHeight);
}
void Game::deinitialise()
{
	//SDL_FreeSurface(message);
	SDL_FreeSurface(g_hero.getSurf());
	SDL_FreeSurface(playerStatusBar);
	SDL_FreeSurface(spaceOver);
	SDL_FreeSurface(spaceBg);
	SDL_FreeSurface(volLabel);
	SDL_FreeSurface(gameOverSurf);
	SDL_FreeSurface(scoreBoard);
//	SDL_FreeSurface(personalBest);
	for(int i=0;i<items.Size();i++)
	{
		items[i].doCleanup();
	}
	for(int i=0;i<lives.Size();i++)
	{
		lives[i].doCleanup();
	}
	for(int i=0;i<trackList.Size();i++)
	{
		Mix_FreeMusic(trackList[i]);
	}
	Mix_FreeMusic(gameOverTR);
	TTF_CloseFont(nameTagfont);
	SDL_FreeSurface(spaceBg);

}
bool Game::loadData()
{
	gameTrack1 = Mix_LoadMUS("..//Data//Game//Sounds//BackGroundMusic//Makki_Theme1.mid");
	gameTrack2 = Mix_LoadMUS("..//Data//Game//Sounds//BackGroundMusic//Makki_Theme2.mid");
	gameTrack3 = Mix_LoadMUS("..//Data//Game//Sounds//BackGroundMusic//Makki_Theme.mid");
	gameOverTR = Mix_LoadMUS("..//Data//Game//Sounds//GameOver.mid");
	if(gameTrack1==NULL || gameTrack2==NULL || gameTrack3==NULL)
		return false;
	trackList.Insert(gameTrack1,0);
	trackList.Insert(gameTrack2,1);
	trackList.Insert(gameTrack3,2);

	nameTagfont = TTF_OpenFont( "..//Data//Login//ARIAL.ttf", 8 );
	if(nameTagfont==NULL )
		return false;

	//
	//for(int i=0;i<20;i++)
	//{
	//	Enemy enemy;
		enemy.doLoad("..//Data//Game//Sprites//Enemies//Enemy.bmp",60,64,4);
		enemy.OscillateAnim(true);
		//enemyList.Append(enemy);
	//}

	for(int i=0;i<items.Size();i++)
	{
		Item itm;
		if(i==0)
		{
		itm.setType(Item::ITM_Health);
		itm.doLoad("..//Data//Game//Sprites//Items//healthBubble.bmp",30,12,5);
		itm.setSoundEffect("..//Data//Game//Sounds//healthPickup.wav");
		}
		if(i==1)
		{
			itm.setType(Item::ITM_Armour);
			itm.doLoad("..//Data//Game//Sprites//Items//ArmourBubble.bmp",30,12,5);
				itm.setSoundEffect("..//Data//Game//Sounds//ArmourPickup.wav");

		}
		if(i==2)
		{
			itm.setType(Item::ITM_Weapon);
			itm.doLoad("..//Data//Game//Sprites//Items//lifeBubble.bmp",30,12,5);
		}
		if(i==3)
		{
			itm.setType(Item::ITM_Life);
			itm.doLoad("..//Data//Game//Sprites//Items//lifeBubble.bmp",30,12,5);
			itm.setSoundEffect("..//Data//Game//Sounds//ExtraLife.wav");
		}
		itm.OscillateAnim(false);
		itm.setAnimFrameRate(150);
		itm.X = rand() % 700 + 50;
		itm.Y = -itm.Height;
		items[i] = itm;
	}
	//initialise variables
	bgY=0;
	soundVolume=20.f;
	scrollspeed=2.4f;
	//start music
	Mix_PlayMusic(trackList[0],-1);
	Mix_Volume(-1,soundVolume);

	return true;
}

bool Game::loadMap()
{
	spaceBg = Sprite::OnLoad("..//Data//Maps//space.bmp");
	spaceOver = Sprite::OnLoad("..//Data//Maps//spaceOVERLAY.bmp");
	if(!introBMP.doLoad("..//Data//Game//Intro.bmp",800,600,9))
	{
		return false;
	}
	for(int i=0;i<10;i++)
	{
		Asteroid a;
		a.doLoad("..//Data//Game//Sprites//Enemies//asteroid.bmp",50,50,3);
		a.Y = -100;
		a.X = 100;
		Asteroid::asteroidList.Append(a);
	}

	doorAlert = Mix_LoadWAV("..//Data//Game//dooropening.wav");
	introBMP.X = 0;
	introBMP.Y = 0;
	introBMP.OscillateAnim(true);
	introBMP.setAnimFrameRate(500);

	Sprite::Transparent(spaceOver,255,0,255);
	spacebgY = 0;
	return true;
}
bool Game::loadPlayerRelated()
{
	if(!g_hero.doLoad("..//Data//Game//Sprites//Player//player.bmp",80,84,4))
	{
		return false;
	}
	g_hero.OscillateAnim(false);
	g_hero.CurrentFrameCol=1;
	g_hero.X = (ScreenWidth - g_hero.getSurf()->w)/2;
	g_hero.Y = 400;
	g_hero.addWeapon(Weapon::WPN_Default);
	g_hero.addWeapon(Weapon::WPN_Rocket);
	g_hero.addWeapon(Weapon::WPN_Laser);
	//PLAYER HUD
	playerStatusBar = Sprite::OnLoad("..//Data//Game//Sprites//Player//healtharmourStatus.bmp");
	if(playerStatusBar==NULL)
	{
		return false;
	}
	Sprite::Transparent(playerStatusBar,255,0,255);

	for(int j=0;j<lives.Size();j++)
	{
		Entity life;
		life.doLoad("..//Data//Game//Sprites//Player//Life.bmp",20,20,0);

		if(life.getSurf()==NULL)
		return false;


		life.X = (ScreenWidth-(life.getSurf()->w+11))-j*(life.getSurf()->w+3);
		life.Y = (ScreenHeight - (playerStatusBar->h)/2)+life.getSurf()->h-2;
		lives[j] = life;
	}

	gameOverSurf = Sprite::OnLoad("..//Data//Game//Sprites//Player//GameOver.bmp");
	if(gameOverSurf==NULL)
		return false;

	return true;
}
int mostNeededPwrUP()
{
	int health = g_hero.getHealth();
	int lives = g_hero.getLives();
	int armour = g_hero.getArmour();

	if(health<40 && lives<2)
	{
		return Item::ITM_Life;
	}
	else if(lives<2)
	{
		return Item::ITM_Life;
	}
	else if(health<40 && lives>=2)
	{
		return Item::ITM_Health;
	}
	else if(armour<30)
	{
		return Item::ITM_Armour;
	}
	else
		return -1;
	
}
int Game::saveInfo()
{
		playerListITR = playerList.GetIterator();
	FILE* fileHandle;
	std::string tmp = "..//Data//Login//USERInfo.txt";
	const char* str = tmp.c_str();
	fileHandle = fopen(str,"w");//open file for writing
	if(fileHandle==NULL)
	{
		return -1;
	}
	bool once=false;
	playerListITR.Start();
	while(playerListITR.Valid())
	{
		Player p = playerListITR.Item();
		if(p.getName()==g_hero.getName())
		{
			p = g_hero;
		}
		std::stringstream ss;
			ss<<p.getName()<<" "<<p.getPass()<<" "<<p.getHealth()<<":"<<p.getArmour()<<":"<<p.getLives()<<"\n";
			once = true;
		std::string tmpSS = ss.str();
		const char* tmpchar = tmpSS.c_str();
		fputs(tmpchar,fileHandle);
		playerListITR.Forth();
	}

	fclose(fileHandle);
}