#ifndef _BULLET_H_
#define _BULLET_H_
#include "Entity.h"

class Bullet : public Entity
{
public:
	enum BulletType{
		BUL_Default=0,BUL_Laser,BUL_Rocket,BUL_None=-1
	};
	Bullet(BulletType);
	~Bullet();

public:
	void doDraw(SDL_Surface* Surf_Display);
	inline BulletType getType(){return _type;}
	inline int getDamage(){return bulletDamage;}
protected:
	int bulletDamage;
	BulletType _type;

};


#endif