#ifndef _STRINGINPUT_H
#define _STRINGINPUT_H

#include <string>
#include <SDL_events.h>
#include "Sprite.h"
#include "Globals.h"
class StringInput
{
private:
	//The storage string
	std::string str;

	//The text surface
	SDL_Surface *text;

public:
	//Initializes variables
	StringInput();
	std::string getStr();
	//Does clean up
	~StringInput();
	//Handles input
	void handle_input(SDL_Event &event);
	void ResetStr();
	void clear_text();

	//Shows the message on screen
	void draw_text(int,int);
};



#endif