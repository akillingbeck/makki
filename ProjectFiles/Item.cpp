#include "Item.h"
bool Item::setPosition;
bool Item::drawItem;
Item::Item()
{
	setPosition=true;
	drawItem=false;

}
Item::~Item()
{
	//Mix_FreeChunk(soundEffect);
}
void Item::setSoundEffect(const char* filename)
{
	soundEffect = Mix_LoadWAV(filename);
	if(soundEffect==NULL)
		isLoaded=false;
	else
		isLoaded=true;
}
void Item::setType(ItemType p_type)
{
	_type = p_type;
}
void Item::doDraw(SDL_Surface* Surf_Display)
{
	if(Surf_Entity == NULL || Surf_Display == NULL) return;

	Sprite::OnDraw(Surf_Display, Surf_Entity,X, Y, CurrentFrameCol * Width, (CurrentFrameRow +Anim_Control.GetCurrentFrame()) * Height, Width, Height);

	Animate();

	if(Y > ScreenHeight)
	{
		setPosition=false;
		drawItem=false;
	}
	if(g_hero.onCollision(getBounds()))
	{
			Mix_PlayChannel( -1, soundEffect, 0 );
		setPosition=false;
		drawItem=false;
//		Y=-Height;
		int health = g_hero.getHealth();
		int lives = g_hero.getLives();
		int armour = g_hero.getArmour();
	switch(_type)
	{
	case ITM_Life:
		{
			
			if(lives!=5)
			g_hero.setLives(1);
			else
			{
				g_hero.setHealth(20);
			if(g_hero.getHealth()>100)
			{
				g_hero.setHealth(100);
			}
			}
			g_hero.setScore(100);
		}
		break;
	case ITM_Armour:
		{	
				g_hero.setArmour(50);
				if(g_hero.getArmour()>100)
				{
					g_hero.setArmour(100);
				}
				g_hero.setScore(20);
		}
		break;
	case ITM_Health:
		{
				g_hero.setHealth(35);
				if(g_hero.getHealth()>100)
				{
					g_hero.setHealth(100);
				}
				g_hero.setScore(20);
		}
		break;
	default:;
	}
	}
}