#ifndef _MISSION_BRIEFING_H
#define _MISSION_BRIEFING_H

#include "Entity.h"
//#include "SceneManager.h"
#include "Globals.h"
#include <SDL_mixer.h>
class MissionBriefing
{
public:
	void initialise();
	void deinitialise();
	void draw();
	void update();
	void handleEvents();
protected:
	TTF_Font* nameFont;
	SDL_Surface* nameInBrief;
	SDL_Surface* pilotName;
	Entity briefing;
	Mix_Music* briefingMus;
private:
	int currentPage;

	
};


#endif