#include "StringInput.h"

namespace {
	int unicodeCount = 0;
}

void requestUnicode()
{
	if(unicodeCount == 0)
	{
		SDL_EnableUNICODE(SDL_ENABLE);
	}
	++unicodeCount;
}

void releaseUnicode()
{
	--unicodeCount;
	if(unicodeCount == 0)
	{
		SDL_EnableUNICODE(SDL_DISABLE);
	}
}

StringInput::StringInput()
{
	//Initialize the string
	str = "";

	//Initialize the surface
	text = NULL;
	//Enable Unicode
	//requestUnicode();
}

StringInput::~StringInput()
{
	//Free text surface
	SDL_FreeSurface( text );

	//Disable Unicode

	releaseUnicode();
}

void StringInput::handle_input(SDL_Event &event)
{
	//If a key was pressed
	if( event.type == SDL_KEYDOWN )
	{
		//Keep a copy of the current version of the string
		std::string temp = str;

		//If the string less than maximum size
		if( str.length() <= 16 )
		{
			
			//If the key is a space
			if( event.key.keysym.unicode == ' ' )
			{
				//Append the character
				str += (char)event.key.keysym.unicode;
			}
			//If the key is a number
			else if( ( event.key.keysym.unicode >= (Uint16)'0' ) && ( event.key.keysym.unicode <= (Uint16)'9' ) )
			{
				//Append the character
				str += (char)event.key.keysym.unicode;
			}
			//If the key is a uppercase letter
			else if( ( event.key.keysym.unicode >= (Uint16)'A' ) && ( event.key.keysym.unicode <= (Uint16)'Z' ) )
			{
				//Append the character
				str += (char)event.key.keysym.unicode;
			}
			//If the key is a lowercase letter
			else if( ( event.key.keysym.unicode >= (Uint16)'a' ) && ( event.key.keysym.unicode <= (Uint16)'z' ) )
			{
				//Append the character
				str += (char)event.key.keysym.unicode;
			}
		}

		//If backspace was pressed and the string isn't blank
		if( ( event.key.keysym.sym == SDLK_BACKSPACE ) && ( str.length() != 0 ) )
		{
			//Remove a character from the end
			str.erase( str.length() - 1 );
		}

		//If the string was changed
		if( str != temp )
		{
			//Free the old surface
			SDL_FreeSurface( text );

			//Render a new text surface
			text = TTF_RenderText_Solid( font, str.c_str(), WHITE );
		}
	}
}

void StringInput::draw_text(int x, int y)
{
	//If the surface isn't blank
	if( text != NULL )
	{
		//Show the name
		Sprite::OnDraw(g_SrWindow,text,x,y);
	}
}
void StringInput::clear_text()
{
	if(text!=NULL)
		text=NULL;
}
std::string StringInput::getStr()
{
	return str;
}
void StringInput::ResetStr()
{
	str = "";
}