#include "Char.h"

Char::Char()
{
	xPos = X;
	yPos = Y;
}
Char::~Char()
{
}
void Char::moveLeft(float timeScale)
{
	if(X>0)
	{
		velocity=PLAYER_SPEED;
		X-=velocity*timeScale;
	}

}
void Char::moveRight(float timeScale)
{

	if(X+Width <= ScreenWidth)
	{
		velocity=PLAYER_SPEED;
		X+=velocity*timeScale;
	}
}
void Char::moveDown(float timeScale)
{
	if(Y+Height<ScreenHeight)
	{
	velocity=PLAYER_SPEED;
	Y+=(velocity*timeScale);
	}

}
void Char::moveUp(float timeScale)
{
	if(Y > ScreenHeight/2)
	{
	velocity = PLAYER_SPEED;
	Y-=(velocity*timeScale);
	}

}
