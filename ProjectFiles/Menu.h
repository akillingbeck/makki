#ifndef _MENU_H_
#define _MENU_H_

#include "Globals.h"
class Menu
{
public:
	Menu();
	~Menu();
	void handleEvents();
	bool initialise();
	void draw();
	void update();

private:
	SDL_Surface* menuScr;

};


#endif