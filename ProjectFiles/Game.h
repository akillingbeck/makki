#ifndef _GAME_H
#define _GAME_H

#include "Item.h"
#include "Player.h"
#include "Array.h"
#include "DLinkedList.h"
#include "SDLHelpers.h"
#include "Globals.h"
#include "Enemy.h"
#include <vector>
#include <sstream>
class Game
{
public:
	Game() : lives(5),items(4),trackList(3){}
	~Game();

	bool initialise();
	bool loadData();
	bool loadMap();
	bool loadPlayerRelated();
	bool timePassed(Uint32,long&);
	int saveInfo();
	void deinitialise();
	void handleEvents(float);
	void draw(float);
	void update(float);
	void drawBG();
	void drawHUD();
	void drawEntities(float);
	float spacebgY;
	float bgY;
	float soundVolume;
	float randX;
	float randY;
	bool GameOverplaying;
	bool introDone;
	bool volChange;
	bool asteroidField;
	int asteroidwave;
	int musicTrack;
	int level;
	int lastScore;
	//int bestScore;
private:
	//Player _hero;
	long astrOLD;
	long pwrupOLD;
	Array<Item> items;
	Array<Mix_Music*>trackList;
	Enemy enemy;
	Array<Entity> lives;
	SDL_Surface* playerStatusBar;
	SDL_Surface* volLabel;
	SDL_Surface* scoreBoard;
	//SDL_Surface* personalBest;
	Mix_Music* gameTrack1;
	Mix_Music* gameTrack2;
	Mix_Music* gameTrack3;
	Mix_Music* gameOverTR;
	Mix_Chunk* doorAlert;
	Entity introBMP;
	SDL_Surface* gameOverSurf;
	SDL_Surface* spaceBg;
	SDL_Surface* spaceOver;
	TTF_Font *nameTagfont;
	SDL_Color textColor;
};


#endif