#ifndef __DEFINES_H__
#define __DEFINES_H__

#define PROGRAM_NAME "Makki Adventures"
#define ScreenWidth 800
#define ScreenHeight 600
#define BPP 32
#define FRAMES_PER_SECOND 100
#define PLAYER_SPEED 4.f

#endif