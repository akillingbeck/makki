#ifndef _WEAPON_H_
#define _WEAPON_H_
#include "DLinkedList.h"
#include "Bullet.h"
#include "Entity.h"
#include <SDL_mixer.h>
class Weapon : public Entity
{
public:
	static DLinkedList<Bullet*> activeBullet;
	static DListIterator<Bullet*> listItr;
public:
	Weapon();
	~Weapon();
public:
	enum WeaponType{
		WPN_Default=0,WPN_Laser,WPN_Rocket
	};
	enum WeaponState{
		STATE_Reload=1,STATE_Empty,STATE_NoState
	};
public:
	//void doDraw(SDL_Surface*,Player);

	float gunPower;

public:
	WeaponState getWpnState();
	int getFireRate();
	inline long getReloadTime(){return reloadTime;}
	int getAmmo();
	void setAmmo();
	Bullet::BulletType getBulletType();
	bool fireWeapon();
	void setType(WeaponType);
	void freeEffects();
	void playEffect(WeaponState);
	
/*public:
	void weapon_type(WeaponType);
	void fireWeapon();*/
protected:
	Mix_Chunk* reloadFX;
	Mix_Chunk* gunSoundFX;
	Mix_Chunk* gunEmpty;
private:
	long reloadTime;
	int numMags;
	int fireRate;
	int magazineSize;
	int currentAmmo;
private:
	WeaponType _type;

};


#endif