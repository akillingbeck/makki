#ifndef _SPRITE_H_
    #define _SPRITE_H_

#include <SDL.h>
#include <SDL_image.h>

class Sprite {
    public:

        Sprite();

    public:
        static SDL_Surface* OnLoad(const char* File);
		static bool OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X, int Y);
		static bool OnDraw(SDL_Surface* Surf_Dest, SDL_Surface* Surf_Src, int X, int Y, int X2, int Y2, int W, int H);
		static bool Transparent(SDL_Surface* Surf_Dest, int R, int G, int B);
};

#endif