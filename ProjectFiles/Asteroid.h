#ifndef _ASTEROID_H_
#define _ASTEROID_H_
#include "Entity.h"
#include "DLinkedList.h"

class Asteroid : public Entity
{
public:
	Asteroid();
	~Asteroid();
	void doDraw(SDL_Surface* Surf_Display);
	bool timePassed(float);
public:
	static DLinkedList<Asteroid> asteroidList;
	static DListIterator<Asteroid> asteroidListItr;
private:
	long oldTime;
	bool positionSet;
};


#endif