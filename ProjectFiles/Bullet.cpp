#include "Bullet.h"
#include "Globals.h"
Bullet::~Bullet()
{

}
Bullet::Bullet(BulletType p_type)
{
	_type = p_type;
	switch(_type)
	{
	case BUL_Default:
		if(doLoad("..//Data//Game//Sprites//Items//Weapons//Default//default.bmp",16,32,4)==false)
		{
			return;
		}
		bulletDamage=10;
		OscillateAnim(true);
		break;
	case BUL_Laser:
		if(doLoad("..//Data//Game//Sprites//Items//Weapons//Laser//laser.bmp",11,32,4)==false)
		{
			return;
		}
		setAnimFrameRate(200);
		bulletDamage=40;
		OscillateAnim(false);
		break;
	case BUL_Rocket:
		if(doLoad("..//Data//Game//Sprites//Items//Weapons//Rocket//rocket.bmp",19,50,4)==false)
		{
			return;
		}
		bulletDamage=80;
		break;
	default:;
	}
	X = (g_hero.X +(g_hero.Width/2))-5;
	Y = g_hero.Y;
}
void Bullet::doDraw(SDL_Surface* Surf_Display)
{
	if(Surf_Entity == NULL || Surf_Display == NULL) return;

	Sprite::OnDraw(Surf_Display, Surf_Entity,X, Y, CurrentFrameCol * Width, (CurrentFrameRow +Anim_Control.GetCurrentFrame()) * Height, Width, Height);
	Animate();

	if(!onScreen())
	{
 	  Weapon::activeBullet.Remove(Weapon::listItr);
		
		SDL_FreeSurface(Surf_Entity);
	}

}