#ifndef __GLOBALS_H__
#define __GLOBALS_H__

#include "SDL.h"
#include "SDL_mixer.h"
#include <SDL_ttf.h>
#include "Defines.h"
#include "DLinkedList.h"
#include "Player.h"
#include <iostream>

class SceneManager;
class Player;

//Structures
extern DLinkedList<Player> playerList;
extern DListIterator<Player> playerListITR;
//SDL
extern float scrollspeed;
extern TTF_Font *font;
extern SDL_Surface *g_SrWindow;
extern Uint8* keystates;
//extern SDL_Event event;
//game info
extern Player g_hero;
extern bool loginComplete;
extern bool gameOver;
extern bool quit;
extern SceneManager sceneManager;

#endif