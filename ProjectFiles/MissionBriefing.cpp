#include "MissionBriefing.h"
#include "SceneManager.h"
void MissionBriefing::initialise()
{
	currentPage=0;

	nameFont = TTF_OpenFont( "..//Data//Login//ARIAL.ttf", 10 );
	std::string tmp = g_hero.getName();
	const char* player_name = tmp.c_str();
	pilotName = TTF_RenderText_Solid(nameFont,player_name,GREEN);
	nameInBrief = TTF_RenderText_Solid(nameFont,player_name,YELLOW);
	briefing.doLoad("..//Data//Maps//Mission_Briefing_1.bmp",800,600,2);
	briefing.setAnimFrame(currentPage);
	briefingMus = Mix_LoadMUS("..//Data//Game//Sounds//Briefing//Mission_1_briefing.mid");
	Mix_PlayMusic(briefingMus,-1);
}
void MissionBriefing::deinitialise()
{
	briefing.doCleanup();
	SDL_FreeSurface(pilotName);
	SDL_FreeSurface(nameInBrief);
	TTF_CloseFont(nameFont);
	Mix_FreeMusic(briefingMus);
}
void MissionBriefing::draw()
{
	briefing.doDraw(g_SrWindow,false);
	if(briefing.getAnimFrame()==0)
	{
		Sprite::OnDraw(g_SrWindow,nameInBrief,298,444);	
	}
	Sprite::OnDraw(g_SrWindow,pilotName,115,340);
}
void MissionBriefing::update()
{
	SDL_Flip(g_SrWindow);
}
void MissionBriefing::handleEvents()
{
	SDL_Event m_event;
	if( SDL_PollEvent( &m_event ) )
	{
		
		if(m_event.type==SDL_KEYDOWN)
		{
			if(m_event.key.keysym.sym==SDLK_RETURN)
			{
				deinitialise();
				sceneManager.setContext(SceneManager::CTX_GAME);
			}
			if(m_event.key.keysym.sym==SDLK_RIGHT)
			{
				if(currentPage<briefing.getMaxFrames())
					currentPage++;

				briefing.setAnimFrame(currentPage);
			}
			if(m_event.key.keysym.sym==SDLK_LEFT)
			{
				if(currentPage>0)
					currentPage--;
				briefing.setAnimFrame(currentPage);
			}
		}
		if(m_event.type==SDL_KEYUP)
		{
			if(m_event.key.keysym.sym==SDLK_RIGHT)
			{

			}
		}

	}
}