//This file holds the Linked Hash Table implementation

#ifndef HASHTABLE_H
#define HASHTABLE_H

#include "DLinkedList.h"
#include "Array.h"

//-----------------------------------------------------------------------
//Name:			HashEntry
//Description:  This is the hash table entry class. It stores
//						 a key and data pair
//-----------------------------------------------------------------------

template<class KeyType, class DataType>
class HashEntry
{
	public:
		KeyType m_key;
		DataType m_data;
};

//--------------------------------------------------------------------------------
//Name:			HashTable
//Description:  This is the hashtable class
//--------------------------------------------------------------------------------

template<class KeyType, class DataType>
class HashTable
{
public:
	//typedef the Entry class to make it easier to work with
	typedef HashEntry<KeyType, DataType> Entry;

//------------------------------------------------------------------------------------
//Name:			HashTable
//Description:  construct the table with a certain size, and a hash 
//						function. The constructor will construct the m_table
//						with the correct size.
//Arguments:  p_size: The size of the table
//						p_hash: The hashing function.
//Return Val:   None
//------------------------------------------------------------------------------------
	HashTable(int p_size, unsigned long int (*p_hash)(KeyType))
		: m_table(p_size)
	{
		//set the size, hash function and count
		m_size = p_size;
		m_hash = p_hash;
		m_count = 0;
	}

//-----------------------------------------------------------------------------------
//Name:			Insert
//Description:  Insert a new Key/Data pair into the table.
//Arguments:	p_key - the key
//						p_data - the data
//Return Val:	None
//-----------------------------------------------------------------------------------

	void Insert(KeyType p_key, DataType p_data)
	{
		//create an entry
		Entry entry;
		entry.m_key = p_key;
		entry.m_data = p_data;

		//get the hash value for the key, and modulo
		//it so that it fits in the table
		int index = m_hash(p_key) % m_size;

		//add the entry at the correct index, increment count
		m_table[index].Append(entry);
		m_count++;
	}

//-----------------------------------------------------------------------------------
//Name:			Find
//Description:  Finds a key in  the table.
//Arguments:	p_key - the key to search for
//Return Val:	a pointer to the entry that has the key/data 
//						or 0 if not found
//-----------------------------------------------------------------------------------

	Entry* Find(KeyType p_key)
	{
		//find the index where the key should exist
		int index = m_hash(p_key) %m_size;

		//get an iterator for the list at that index
		DListIterator<Entry> itr = m_table[index].GetIterator();

		//search each item
		while(itr.Valid())
		{
			//if the keys match, then return a pointer to the entry
			if(itr.Item().m_key == p_key)
				return &(itr.Item());
			itr.Forth();
		}

		//the key wasn't found, return 0.
		return false;
	}

//-----------------------------------------------------------------------------------
//Name:			Remove
//Description:  Remove entry based on a key.
//Arguments:	p_key - the key to remove
//Return Val:	true if removed, false if not found
//-----------------------------------------------------------------------------------

	bool Remove(KeyType p_key)
	{
		//find the index where the key should be
		int index = m_hash(p_key) %m_size;

		//get an iterator for the list at that index
		DListIterator<Entry> itr = m_table[index].GetIterator();

		//search for the item
		while(itr.Valid())
		{
			//if the keys match, remove the node, and return true
			if(itr.Item().m_key == p_key)
			{
				m_table[index].Remove(itr);
				m_count--;
				return true;
			}
			itr.Forth();
		}

		//item wasn't found
		return false;
	}

	int Count()
	{
		return m_count;
	}

	int m_size;
	int m_count;
	Array<DLinkedList<Entry>> m_table;
	unsigned long int (*m_hash)(KeyType);
};

#endif

