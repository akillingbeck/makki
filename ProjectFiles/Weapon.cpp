#include "Weapon.h"

DLinkedList<Bullet*> Weapon::activeBullet;
DListIterator<Bullet*> Weapon::listItr;
Weapon::Weapon()
{
	reloadFX = Mix_LoadWAV("..//Data//Game//Sounds//reload.wav");
	gunEmpty = Mix_LoadWAV("..//Data//Game//Sounds//empty.wav");
}
Weapon::~Weapon()
{

}
Weapon::WeaponState Weapon::getWpnState()
{
	if(currentAmmo>0&&numMags>0)
	{
	return STATE_NoState;
	}
	else if(currentAmmo<1)
	{
		if(numMags>0)
		{
 			numMags--;
			if(numMags!=0)
			{
		currentAmmo = magazineSize;
		return STATE_Reload;
			}
			else
				return STATE_Empty;
		}
		else
		{currentAmmo=0;
			return STATE_Empty;
		}
		
	}
}
void Weapon::freeEffects()
{
	Mix_FreeChunk(gunSoundFX);
	Mix_FreeChunk(reloadFX);
}
void Weapon::playEffect(WeaponState wep)
{
	switch(wep)
	{
	case STATE_Empty:
		if(gunEmpty!=NULL)
		{
			Mix_PlayChannel(-1,gunEmpty,0);
		}
		else
			return;
		break;
	case STATE_Reload:
		{
			if(reloadFX!=NULL)
			{
			Mix_PlayChannel(-1,reloadFX,0);
			}
			else
				return;
		}
		break;
	case STATE_NoState:
		{
			if(gunSoundFX!=NULL)
			{
			Mix_PlayChannel(-1,gunSoundFX,0);
			}
			else
				return;
		}
		break;
	}
}
bool Weapon::fireWeapon()
{
	if(currentAmmo>0)
	{
		currentAmmo--;
		return true;
	}
	return false;
}
int Weapon::getFireRate()
{
	return fireRate;
}
int Weapon::getAmmo()
{
	return currentAmmo;
}
void Weapon::setType(WeaponType p_type)
{
	_type = p_type;
	switch(_type)
	{
	case WPN_Default:
		{
		doLoad("..//Data//Game//Sprites//Items//Weapons//Default//defaultICO.bmp",30,60,0);
		magazineSize = 999;
		currentAmmo= 999;
		numMags=999;
		reloadTime=4;
		fireRate = 8;
		gunPower = 1.5f;
		gunSoundFX = Mix_LoadWAV("..//Data//Game//Sounds//Laser_normal.wav");
		}
		break;
	case WPN_Laser:
		doLoad("..//Data//Game//Sprites//Items//Weapons//Laser//laserICO.bmp",30,60,0);
		magazineSize = 30;
		currentAmmo = 30;
		numMags=3;
		fireRate = 4;
		reloadTime=4;
		gunPower=0.8f;
		gunSoundFX = Mix_LoadWAV("..//Data//Game//Sounds//laser_blast.wav");
		break;
	case WPN_Rocket:
		doLoad("..//Data//Game//Sprites//Items//Weapons//Rocket//rocketICO.bmp",30,60,0);
		gunSoundFX = Mix_LoadWAV("..//Data//Game//Sounds//Rocket.wav");
		magazineSize = 10;
		currentAmmo = 10;
		fireRate = 2;
		numMags=2;
		reloadTime=3;
		gunPower = 0.6f;
		break;
	default:;

	}
}
Bullet::BulletType  Weapon::getBulletType()
{
	if(_type==WPN_Default)
	{
		return Bullet::BUL_Default;
	}
	if(_type==WPN_Laser)
	{
		return Bullet::BUL_Laser;
	}
	if(_type==WPN_Rocket)
	{
		return Bullet::BUL_Rocket;
	}
	Bullet::BUL_None;
}