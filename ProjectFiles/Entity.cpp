#include "Entity.h"
//DLinkedList<Entity> Entity::entityList;
//DListIterator<Entity> Entity::entityItr;
Entity::Entity() {
    Surf_Entity = NULL;

	X = 0;
	Y = 0;

	Width 	= 0;
	Height 	= 0;

	CurrentFrameCol = 0;
	CurrentFrameRow = 0;

    AnimState = 0;
}

Entity::~Entity() {

}
SDL_Rect Entity::getBounds()
{
	bounds.w = Width;
	bounds.h = Height;
	bounds.x = X;
	bounds.y = Y;
	return bounds;
}
bool Entity::doLoad(char* File, int Width, int Height, int MaxFrames) {
    if((Surf_Entity = Sprite::OnLoad(File)) == NULL) {
        return false;
    }

    Sprite::Transparent(Surf_Entity, 255, 0, 255);

    this->Width = Width;
    this->Height = Height;

    Anim_Control.MaxFrames = MaxFrames;

    return true;
}
void Entity::OscillateAnim(bool oscillate)
{
	Anim_Control.Oscillate = oscillate;
}
void Entity::Animate() {
    Anim_Control.OnAnimate();
}
void Entity::setAnimFrame(int frame)
{
	Anim_Control.SetCurrentFrame(frame);
}
void Entity::setAnimFrameRate(int frame)
{
	Anim_Control.SetFrameRate(frame);

}
int Entity::getAnimFrame()
{

	return Anim_Control.GetCurrentFrame();
}
void Entity::doDraw(SDL_Surface* Surf_Display,bool animate) {
    if(Surf_Entity == NULL || Surf_Display == NULL) return;

		Sprite::OnDraw(Surf_Display, Surf_Entity,X, Y, CurrentFrameCol * Width, (CurrentFrameRow +Anim_Control.GetCurrentFrame()) * Height, Width, Height);

		if(animate)
	Animate();
}
void Entity::doCleanup() {
    if(Surf_Entity) {
        SDL_FreeSurface(Surf_Entity);
    }

    Surf_Entity = NULL;
}
bool Entity::onScreen()
{
	if(X <0 || X > ScreenWidth)
		return false;
	if(Y < 0 || Y > ScreenHeight)
		return false;

	return true;
}
 
SDL_Surface* Entity::getSurf()
{
	return Surf_Entity;
}
bool Entity::onCollision(SDL_Rect rect1)//, Entity &ent2)
{
	//SDL_Rect rect1 = ent1.getBounds();
	//SDL_Rect rect2 = ent2.getBounds();
	
	//The sides of the rectangles 
	int leftA, leftB; 
	int rightA, rightB;
	int topA, topB; 
	int bottomA, bottomB; //Calculate the sides of rect A 
	leftA = (int)rect1.x; 
	rightA = (int)rect1.x + rect1.w; 
	topA = (int)rect1.y; 
	bottomA = (int)rect1.y + rect1.h; //Calculate the sides of rect B
	leftB = (int)X; 
	rightB = (int)X + Width; 
	topB = (int)Y;
	bottomB = (int)Y+ Height;
	//If any of the sides from A are outside of B 
	if( bottomA <= topB ) { return false; } 
	if( topA >= bottomB ) { return false; }
	if( rightA <= leftB ) { return false; } 
	if( leftA >= rightB ) { return false; } 
	//If none of the sides from A are outside B 
	return true; }
	
