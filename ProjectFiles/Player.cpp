#include "Player.h"
#include "Globals.h"
Player::Player()
{
	Weapon::listItr = Weapon::activeBullet.GetIterator();
	inventoryIndex = 0;
	numLives=0;
//	personalBest=0;
	score=0;
	health=0;
	armour=0;
	//inventoryITR = inventory.GetIterator();
}
Player::~Player()
{
	doCleanup();
}
void Player::addWeapon(Weapon::WeaponType p_weapon)
{
	Weapon w;
	w.setType(p_weapon);
	w.X = w.Width * inventory.size();
	w.Y = ScreenHeight - w.Height;
		//inventory.Append(w);
	//inventory.Append(p_weapon);
	/*The inventory can basically be seen as an array of size 3, there are currently only 3 weapons.
	each time this function is called it adds the weapon to the next spot*/
	inventory.push_back(w);
}
/*previous and next weapon simple changes the index number for the array*/
void Player::previousWeapon()
{

		inventoryIndex--;
		if(inventoryIndex<0)
			inventoryIndex=inventory.size()-1;
}
void Player::nextWeapon()
{
		inventoryIndex++;
		if(inventoryIndex>inventory.size()-1)
			inventoryIndex=0;
}
void Player::doDraw(SDL_Surface* Surf_Display) {
	if(Surf_Entity == NULL || Surf_Display == NULL) return;

	Weapon::listItr.Start();
	/*The weapon class contains a static DLinkedList for all the activebullets
	and active bullet is one that is on screen and that is not collided with anything
	this bit of code will iterate through each bullet in the list. A bullet is added to the list, everytime
	the fire function below is called.(when the player shoots). If the bullet is no longer on screen/if it
	collides with anything, it is removed from the list....The complexity here will be O(n) for drawing but O(c) for
	removing and adding bullets*/
	while(Weapon::listItr.Valid())
	{
		Weapon::listItr.Item()->Y-=inventory[inventoryIndex].gunPower;
		Weapon::listItr.Item()->doDraw(Surf_Display);
		Weapon::listItr.Forth();
	}
	Sprite::OnDraw(Surf_Display, Surf_Entity,X, Y, CurrentFrameCol * Width, (CurrentFrameRow +Anim_Control.GetCurrentFrame()) * Height, Width, Height);
	

	for(int i=0;i<inventory.size();i++)
	inventory[i].doDraw(Surf_Display);

	SDLArrowLine(Surf_Display,inventory[inventoryIndex].X+inventory[inventoryIndex].Width/2,inventory[inventoryIndex].Y-inventory[inventoryIndex].Height,
		inventory[inventoryIndex].X+inventory[inventoryIndex].Width/2,inventory[inventoryIndex].Y,20,5,false,true,YELLOW);
	//SDLBox(Surf_Display,inventory[inventoryIndex].X,inventory[inventoryIndex].Y - 5,inventory[inventoryIndex].Width,3,YELLOW);

	if(!reloaded)
	{
		reloading.doDraw(g_SrWindow);
		if(timePassed(inventory[inventoryIndex].getReloadTime()))
		{
			inventory[inventoryIndex].playEffect(Weapon::STATE_Reload);
			reloaded=true;
		}
	}
	if(inventory[inventoryIndex].getAmmo()<200)
	SDLBox(Surf_Display,inventory[inventoryIndex].X,ScreenHeight - 10,inventory[inventoryIndex].getAmmo(),10,GREEN);
	else
		SDLBox(Surf_Display,inventory[inventoryIndex].X,ScreenHeight - 10,inventory[inventoryIndex].Width,10,BLUE);
	Animate();
}
void Player::fire(float timeScale)
{
	/*Each weapon has a certain bullet associated with it, and also all of the other properties such as
		reload time,fire rate, magazine size, the Weapon state. 
		To access the property of the current weapon therefore will be O(c), Each time the weapon is changed using
		the above functions, an int variable is increased/decreased and therefore I can access the current weapon*/
	if(Weapon::activeBullet.Size() < inventory[inventoryIndex].getFireRate())
	{
	if(!reloaded)
	{

		if(timePassed(inventory[inventoryIndex].getReloadTime()))
		{
			Bullet *bul = new Bullet(inventory[inventoryIndex].getBulletType());
			Weapon::activeBullet.Append(bul);
			inventory[inventoryIndex].fireWeapon();
			reloaded=true;
		}
	}
	else
	{
	switch(inventory[inventoryIndex].getWpnState())//finds out if the weapon is reloading/empty/can fire
	{
	case Weapon::STATE_NoState:
		{
		Bullet *bul = new Bullet(inventory[inventoryIndex].getBulletType());//gets the bullet type assoiciated with the weapon
		    Weapon::activeBullet.Append(bul);
			inventory[inventoryIndex].fireWeapon();
				inventory[inventoryIndex].playEffect(Weapon::STATE_NoState);

		}
			break;
	case Weapon::STATE_Reload:
		{
			reloading.doCleanup();
			reloading.doLoad("..//Data//Game//Sprites//Reloading.bmp",600,200,2);
			reloading.OscillateAnim(true);
			reloading.setAnimFrameRate(200);
			reloading.X = (ScreenWidth - reloading.Width)/2;
			reloading.Y = (ScreenHeight - reloading.Height)/2;
		oldTime=SDL_GetTicks();
		reloaded=false;
		}
		break;
	case Weapon::STATE_Empty:
		inventory[inventoryIndex].playEffect(Weapon::STATE_Empty);
		break;
	default:;
	}
	}
	}
	//}
	//}
}
void Player::setLives(int lives)
{
	numLives += lives;
	if(numLives>5)
		numLives=5;
	if(numLives<0)
	{
		gameOver=true;
	}
}
void Player::setHealth(int p_health)
{
	health += p_health;
	if(health<0 && numLives<=0)
	{
		gameOver=true;
	}
	if(health>100)
	{
		health = 100;
	}
	else if(health<0&&numLives>0)
	{
		health=100;
		numLives--;
	}
}
void Player::setArmour(int p_armour)
{
	armour += p_armour;
	if(armour>0)
	{
		setHealth(p_armour/5);
	}
	else if(armour < 0)
	{
		armour=0;
		setHealth(p_armour);
	}
}
int Player::getLives()
{
	return numLives;
}
int Player::getHealth()
{
	return health;
}
int Player::getArmour()
{
	return armour;
}

void Player::setName(std::string name)
{
	userName = name;
}
void Player::setPass(std::string pass)
{

	userPass = pass;
}
std::string Player::getName()
{
	return userName;
}
std::string Player::getPass()
{
	return userPass;
}
/*void Player::setPersonalBest(int newScore)
{
	if(newScore > personalBest)
		personalBest=newScore;
}*/
void Player::setScore(int p_score)
{
	score+=p_score;
}
bool Player::timePassed(Uint32 timePass)
{
	Uint32 desiredTime = timePass*1000;
	if(SDL_GetTicks()-oldTime>desiredTime)
	{
		oldTime = SDL_GetTicks();
		return true;
	}
	return false;
}
void Player::resetPlayer()
{
	Weapon::listItr.Start();
	numLives = 5;
	armour=100;
	health=100;
	while(Weapon::listItr.Valid())
	{
	Weapon::activeBullet.Remove(Weapon::listItr);
	Weapon::listItr.Forth();
	}
}