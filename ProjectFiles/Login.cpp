#include "Login.h"
#include "Globals.h"
#include "SceneManager.h"
#include "Player.h"
#include <sstream>
#include <fstream>

/*------------------------------function prototypes--------------------------------------*/
int checkLogin(std::string,std::string);
bool loadUserData();
void drawMenu();
int saveUserToFile(Player);
unsigned long int StringHash(std::string p_string);
/*-------------------------------GLobal variables---------------------------------------------------*/
HashTable<std::string, Player> table(31,StringHash);
HashEntry<std::string, Player>* entry;
Login::~Login()
{
	//deinitialise();
}

bool Login::initialise()
{
	keystates = SDL_GetKeyState(0);
	
	headerFont = TTF_OpenFont( "..//Data//Login//ARIAL.ttf", 40 );
	/*----------------Initialise flags----------------------*/
	{
		removeInfo = true;
		showInfo = false;
		regSuccess=false;
		loginScreen=false;
		registerScreen=false;
		regFailed=false;
	nameEntered=false;
	passEntered=false;
	noUsernameFound=false;
	wrongPass = false;
	failed = false;
	}
	newTime = SDL_GetTicks();
	/*load sprites,fill the hashtable, load music tracks*/
	if(!loadData())
		return false;
	 /*--------------Initialise Music---------------------*/
	{
	musicTrack=0;
	musicVol=20;
	Mix_PlayMusic(track1,-1);
	Mix_VolumeMusic(musicVol);
	}

	/*-------------TEXT SURFACES-----------------------*/
	{
	nameMsg = TTF_RenderText_Solid(font, "Enter Name:", GREEN );
	passMsg = TTF_RenderText_Solid(font, "Enter Pass:", GREEN );
	loginFailMsg = TTF_RenderText_Solid(font,"Login Failed, please retry",RED);
	wrongPassMsg = TTF_RenderText_Solid(font,"Password does not match",RED);
	userNotFoundMsg = TTF_RenderText_Solid(font,"User name not found",RED);
	simpleMenu = TTF_RenderText_Solid(font,"F1:Help   F2:Login   F3:Register   F4:Exit",WHITE);
	volLabel = TTF_RenderText_Solid(font,"VOLUME:",GREEN);
	menuHeading = 0;
	}
	return true;
}
void Login::draw()
{
	//float temp = musicVol;

	Sprite::OnDraw(g_SrWindow,login_Bg,0,0); 
	if(volChange)
	{
		Sprite::OnDraw(g_SrWindow,volLabel,(ScreenWidth - volLabel->w)/2,(ScreenHeight - volLabel->h));
		SDLBox(g_SrWindow,(ScreenWidth - volLabel->w)/2 + (volLabel->w),(ScreenHeight - volLabel->h)+10,(int)musicVol*5,14,GREEN);
	}

	drawMenu();
	if(!removeInfo)
	{
		//Sprite::OnDraw(g_SrWindow,title,100,100);
		title.doDraw(g_SrWindow);
		
		
		if(infoScreen.getAnimFrame()!=4)
		{
			infoScreen.OscillateAnim(true);
			infoScreen.doDraw(g_SrWindow);
		infoScreen.setAnimFrameRate(100);
		}else{
			infoScreen.doDraw(g_SrWindow);
			infoScreen.setAnimFrame(4);
		}
		
		//infoScreen.setAnimFrame(4);
		showInfo=true;
	}
	else
	{
		showInfo=false;
		
		infoScreen.setAnimFrame(0);
	if(loginScreen)//if user wants to login
	{
	drawLogin();
	SDLLine(g_SrWindow,30,400,770,400,GREY);
	SDLLine(g_SrWindow,30,400,30,500,GREY);
	SDLLine(g_SrWindow,770,400,770,500,GREY);
	SDLLine(g_SrWindow,30,500,770,500,GREY);

	SDLLine(g_SrWindow,25,395,775,395,GREY);
	SDLLine(g_SrWindow,25,395,25,505,GREY);
	SDLLine(g_SrWindow,775,395,775,505,GREY);
	SDLLine(g_SrWindow,25,505,775,505,GREY);
	}

	if(registerScreen)
	{
		drawRegister();
		SDLLine(g_SrWindow,30,400,770,400,GREY);
		SDLLine(g_SrWindow,30,400,30,500,GREY);
		SDLLine(g_SrWindow,770,400,770,500,GREY);
		SDLLine(g_SrWindow,30,500,770,500,GREY);

		SDLLine(g_SrWindow,25,395,775,395,GREY);
		SDLLine(g_SrWindow,25,395,25,505,GREY);
		SDLLine(g_SrWindow,775,395,775,505,GREY);
		SDLLine(g_SrWindow,25,505,775,505,GREY);
	}

	//Sprite::OnDraw(g_SrWindow,title,100,100);
	title.doDraw(g_SrWindow);
	}
	
}
void Login::update()
{
	SDL_Flip(g_SrWindow);

}
void Login::handleEvents()
{
	volChange=false;


	/*Volume control---------------------------------------*/
	if(keystates[SDLK_MINUS])
	{
		volChange=true;
		if(musicVol>0)
		{
		musicVol-=0.05f;
		Mix_VolumeMusic(musicVol);
		}
	}
	if(keystates[SDLK_EQUALS])
	{
		volChange=true;
		if(musicVol<40)
		{
			musicVol+=0.05f;
			Mix_VolumeMusic(musicVol);
		}
	}
	/*-----------------------------------------------------------*/
	SDL_Event m_event;
	
	if( SDL_PollEvent( &m_event ) )
	{
		if(loginScreen || registerScreen)//if user is on the login or register "menu"
		{
				if(!nameEntered)
				{
				name.handle_input(m_event);
				}
				else if(!passEntered)
				{
					password.handle_input(m_event);
				}
		}
		/*-------------------------------MENU CONTROLS------------------------------------------*/
		if(m_event.type==SDL_KEYDOWN)
		{

			if(m_event.key.keysym.sym==SDLK_PAUSE)
			{
				Mix_HaltMusic();
			}
			if(m_event.key.keysym.sym==SDLK_PAGEUP)
			{
				if(musicTrack!=2)
				{
					musicTrack++;
				}
				Mix_PlayMusic(trackList[musicTrack],-1);
			}
			if(m_event.key.keysym.sym==SDLK_PAGEDOWN)
			{
				if(musicTrack!=0)
				{
					musicTrack--;
				}
				Mix_PlayMusic(trackList[musicTrack],-1);
			}


		if(m_event.key.keysym.sym==SDLK_F1)
		{
			Mix_PlayChannel( -1, changeMenu, 0 );
			if(showInfo)
			{
			removeInfo = true;
			}
			if(!showInfo)
			{
				removeInfo = false;
			}
		}
	
		if(m_event.key.keysym.sym==SDLK_F2)
		{
			Mix_PlayChannel( -1, changeMenu, 0 );
			loginScreen=true;
			registerScreen=false;
		}
		if(m_event.key.keysym.sym == SDLK_F3)
		{
			Mix_PlayChannel( -1, changeMenu, 0 );
			registerScreen=true;
			loginScreen = false;
		}
		if(m_event.key.keysym.sym==SDLK_F4)
		{
			Mix_PlayChannel( -1, changeMenu, 0 );
			quit=true;
		}
		}
		/*------------------------------END MENU CONTROLS------------------------------------------*/
		if(m_event.type==SDL_KEYUP)
		{

			if(m_event.key.keysym.sym==SDLK_RETURN)
			{
			SDL_Delay(500);
			noUsernameFound=false;
			wrongPass=false;
			regSuccess=false;
			regFailed=false;
			failed=false;
				
			}
		}
		if(m_event.type==SDL_KEYDOWN)
		{
		if(m_event.key.keysym.sym==SDLK_RETURN)
		{
			//delay to show message
			
			//reset flags
			
			if(loginScreen)//if the login screen is active
			{
					if(doLogin())
					{
						entry = table.Find(name.getStr());
						g_hero = entry->m_data;
						name.ResetStr();
						password.ResetStr();
						name.clear_text();
						password.clear_text();
						deinitialise();
						sceneManager.setContext(SceneManager::CTX_BRIEFING);
					}
			}
			else if(registerScreen)//if register screen is active
			{
					if(doRegister())
					{
						regSuccess=true;
					}
			}
			if(name.getStr()!="")
			{
				nameEntered=true;
			}else
			{
				nameEntered=false;
			}
			if(password.getStr()!="")
			{
				passEntered=true;
			}
			else
			{
				passEntered=false;
			}
			
		}
		}

		if(m_event.type==SDL_QUIT)
		{
			quit = true;
		}

	}


}
/*---------------------------------------START OF REGISTRATION/LOGIN CODE----------------------------------------*/
/*------------------------------------Functions from Login Class-------------------------------------------------*/
bool Login::doRegister()
{

	if((name.getStr()!="")&&(password.getStr()!=""))
	{
		Player p;
		p.setName(name.getStr());
		p.setPass(password.getStr());
		p.setLives(5);
		p.setArmour(50);
		p.setHealth(100);
		if(saveUserToFile(p)==-1)
		{
			name.ResetStr();
			password.ResetStr();
			regFailed=true;
			Mix_PlayChannel( -1, errorFX, 0 );
			return false;

		}
		else
		{
			table.Insert(p.getName(),p);
			playerList.Append(p);
			name.ResetStr();
			password.ResetStr();
			return true;
		}

	}
	return false;
}
//-------------------------------------------------------------------
bool Login::doLogin()
{
	if((name.getStr()!="") && (password.getStr()!=""))
	{
		int result = checkLogin(name.getStr(),password.getStr());
		if(result==-1)//username not found
		{
			name.ResetStr();
			password.ResetStr();
			noUsernameFound = true;
			failed = true;
			Mix_PlayChannel( -1, errorFX, 0 );
			return false;
		}
		else if(result == 0)//password doesnt match
		{
			name.ResetStr();
			password.ResetStr();
			wrongPass = true;
			failed=true;
			Mix_PlayChannel( -1, errorFX, 0 );
			return false;
		}
		else{
			return true;
		}
	}
	return false;

}
/*------------------------------------------------LOGIN/REGISTER DRAW FUNCTIONS-------------------------------------*/
void Login::drawLogin()
{
	SDL_FreeSurface(menuHeading);
	menuHeading = TTF_RenderText_Solid(headerFont,"LOGIN",WHITE);
	Sprite::OnDraw(g_SrWindow,nameMsg,50,400);
	Sprite::OnDraw(g_SrWindow,passMsg,50,450);
	Sprite::OnDraw(g_SrWindow,menuHeading,(ScreenWidth - menuHeading->w)/2,340);

	if(failed)
	{
		name.clear_text();
		password.clear_text();
		Sprite::OnDraw(g_SrWindow,loginFailMsg,(ScreenWidth - (loginFailMsg->w))/2,550);
		if(noUsernameFound)
		{
			Sprite::OnDraw(g_SrWindow,userNotFoundMsg,(ScreenWidth - (userNotFoundMsg->w))/2,500);
		}
		if(wrongPass)
		{
			Sprite::OnDraw(g_SrWindow,wrongPassMsg,(ScreenWidth - (wrongPassMsg->w))/2,500);
		}
	}
	name.draw_text((nameMsg->w)+50,400);	
	password.draw_text((passMsg->w)+50,450);
}
//--------------------------------------------------------------
void Login::drawRegister()
{
	SDL_FreeSurface(menuHeading);
	menuHeading = TTF_RenderText_Solid(headerFont,"REGISTER",WHITE);
	if(regSuccess)
	{
		SDL_FreeSurface(menuHeading);
		menuHeading = TTF_RenderText_Solid(headerFont,"REGISTRATION COMPLETE",BLUE);
		name.clear_text();
		password.clear_text();
	}
	if(regFailed)
	{
		SDL_FreeSurface(menuHeading);
		menuHeading = TTF_RenderText_Solid(headerFont,"USER EXISTS WITH THAT NAME",RED);
		name.clear_text();
		password.clear_text();
	}
	Sprite::OnDraw(g_SrWindow,nameMsg,50,400);
	Sprite::OnDraw(g_SrWindow,passMsg,50,450);
	Sprite::OnDraw(g_SrWindow,menuHeading,(ScreenWidth - menuHeading->w)/2,340);
	name.draw_text((nameMsg->w)+50,400);	
	password.draw_text((passMsg->w)+50,450);
	
}
/*------------------------------------------------LOGIN HELPERS----------------------------------------------*/
int checkLogin(std::string name,std::string pass)
{
	/*searches for data which contains the key
	Best case scenario is O(c)....I made the hashtable a size of 31(prime number)
	if the name is found, the password is checked*/
	entry = table.Find(name);
	if(entry==0)
	{
	return -1;
	}
	else
	{
		if(entry->m_data.getPass()==pass)
		{
		return 1;
		}
		return 0;
	}
}
//For registering
int saveUserToFile(Player player)
{
	int result = checkLogin(player.getName(),player.getPass());
	if(result==0 || result == 1)
	{

		return -1;
	}
	else
	{
		long size;
		FILE* fileHandle;
		std::string tmp = "..//Data//Login//USERInfo.txt";
		const char* str = tmp.c_str();
		fileHandle = fopen(str,"a+");//open file for appending
		if(fileHandle==NULL)
		{
			return -1;
		}
		std::stringstream ss;
	/*	if(size==0)
		{
			ss<<player.getName()<<" "<<player.getPass()<<" "<<player.getHealth()<<":"<<player.getArmour()<<":"<<player.getLives();
		}
		else
		{*/
			ss<<player.getName()<<" "<<player.getPass()<<" "<<player.getHealth()<<":"<<player.getArmour()<<":"<<player.getLives()<<"\n";
		//}
		
		
		//ss<<"\n"<<player.getName()<<" "<<player.getPass();
		std::string tmpSS = ss.str();
		const char* tmpchar = tmpSS.c_str();
		fputs(tmpchar,fileHandle);
		
		fclose(fileHandle);
		return 1;
	}
}
/*-------------------------------------------------END LOGIN CODE-------------------------------------------*/
bool Login::loadData()
{

	infoScreen.doLoad("../Data/Login/Images/HelpScreen.bmp",499,495,5);
	infoScreen.X = (ScreenWidth - 499)/2;
	infoScreen.Y = (ScreenHeight - 495)/2;
	//Images------------------
	login_Bg = Sprite::OnLoad("../Data/Login/Images/Login_bg.bmp");
	//title = Sprite::OnLoad("../Data/Login/Images/MakkiTitle.bmp");
	title.doLoad("../Data/Login/Images/MakkiTitle2.bmp",600,200,2);
	title.X = 100;
	title.Y = 100;
	title.OscillateAnim(true);
	if(login_Bg==NULL || title.getSurf() ==NULL)
		return false;
	//Tracks-----------------
	track1 = Mix_LoadMUS("../Data/Login/Sounds/titleTR1.mid");
	track2 = Mix_LoadMUS("../Data/Login/Sounds/titleTR2.mid");
	track3 = Mix_LoadMUS("../Data/Login/Sounds/titleTR3.mid");
	//Sound effects------------------------------
	changeMenu = Mix_LoadWAV("../Data/Login/Sounds/changescreen.wav");
	errorFX = Mix_LoadWAV("../Data/Login/Sounds/error.wav");
	if(changeMenu==NULL || errorFX==NULL)
		return false;

	if(track1==NULL||track2==NULL||track3==NULL)
		return false;
	trackList.Insert(track1,0);
	trackList.Insert(track2,1);
	trackList.Insert(track3,2);
	//Fill hashtable------------------------
	if(loadUserData()==false)
	{
		return false;
	}

	return true;
}
bool loadUserData()
{
	FILE* fileHandle;
	std::string tmp = "..//Data//Login//USERInfo.txt";
	const char* str = tmp.c_str();
	fileHandle = fopen(str,"r");
	if(fileHandle==NULL)
	{
		return false;
	}
	/*std::string tmpname = "";
	std::string tmppass = "";
	const char* name = tmpname.c_str();
	const char* pass = tmppass.c_str();*/
	char name[256];  
	char pass[256];
	int health = -1;
	int armour = -1;
	int lives = -1;

	while( true ) {
		Player user;
		int vals_read = fscanf(fileHandle,"%s %s %d:%d:%d",name,pass,&health,&armour,&lives);
		if( vals_read == 5 ) {
			// process the line
			user.setName(name);
			user.setPass(pass);
			user.setHealth(health);
			user.setArmour(armour);
			user.setLives(lives);
			table.Insert(user.getName(),user);
			playerList.Append(user);
			//best case is O(c) for insert depending on if a collision occurs or not
		} else if( feof( fileHandle ) )
			break;
	}
	fclose(fileHandle);
	return true;
}

void Login::drawMenu()
{
	Sprite::OnDraw(g_SrWindow,simpleMenu,0,0);
}/*------------------------------------------------CLEAN UP------------------------------------------------*/
void Login::deinitialise()
{
//	SDL_FreeSurface(title);
	SDL_FreeSurface(login_Bg);

	SDL_FreeSurface(nameMsg);
	SDL_FreeSurface(passMsg);
	SDL_FreeSurface(loginFailMsg);
	SDL_FreeSurface(userNotFoundMsg);
	SDL_FreeSurface(wrongPassMsg);
	SDL_FreeSurface(volLabel);
	SDL_FreeSurface(simpleMenu);
	SDL_FreeSurface(menuHeading);
	title.doCleanup();
	infoScreen.doCleanup();
	TTF_CloseFont(headerFont);
	Mix_FreeChunk(changeMenu);
	Mix_FreeChunk(errorFX);

	for(int i=0;i<trackList.Size();i++)
		Mix_FreeMusic(trackList[i]);
	
	//delete[] trackList;

}
bool Login::timePassed(Uint32 timePass, long &oldtime)
{
	Uint32 desiredTime = timePass*1000;
	if(SDL_GetTicks()-oldtime>desiredTime)
	{
		oldtime = SDL_GetTicks();
		return true;
	}
	return false;
}


unsigned long int StringHash(std::string p_string)
{
	unsigned long int hash = 0;
	int i;
	int length = p_string.length();
	for(i=0;i<length;i++)
	{
		hash += ((i+1)*p_string[i]);
	}
	return hash;
}

