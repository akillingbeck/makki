#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "Char.h"
#include "Globals.h"

class Enemy : public Char
{
public:
	Enemy();
	~Enemy();
public:
	void doDraw(SDL_Surface* Surf_Display,float);
	bool setPosition;
	bool collision;
	void setHealth(int);
	void setArmour(int);
	inline int getHealth(){return health;}
	inline int getArmour(){return armour;}
protected:
	int health;
	int armour;

};


#endif