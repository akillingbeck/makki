#ifndef _PLAYER_H_
#define _PLAYER_H_
#include "Char.h"
#include "Weapon.h"
#include "Array.h"
#include "SDLHelpers.h"

class Player : public Char
{
public:
	Player() ;//: weapons(3){}//{Weapon::listItr=Weapon::activeBullet.GetIterator();}//,health=100,lives=5,armour=50;
	~Player();
	int getLives();
	int getHealth();
	int getArmour();
	std::string getName();
	std::string getPass();
	void setName(std::string);
	void setPass(std::string);
	
	void setLives(int);
	void setHealth(int);
	void setArmour(int);
	void fire(float);
	void doDraw(SDL_Surface*);
	void addWeapon(Weapon::WeaponType);
	void nextWeapon();
	void previousWeapon();
	bool timePassed(Uint32);
	void resetPlayer();
	void setScore(int);
	//inline int getPersonalBest(){return personalBest;}
	//void setPersonalBest(int);
	inline int getScore(){return score;}
protected:
	int personalBest;
		int score;
	int numLives;
	int health;
	int armour;
	long oldTime;
	bool reloaded;
private:
	Entity reloading;
	int inventoryIndex;
	std::vector<Weapon> inventory;
	std::string userName;
	std::string userPass;
};


#endif